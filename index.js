let http = require("http");


http.createServer(function(request, response) {
	// GET Request
	if(request.url == "/" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to Booking System');
	}

	// POST Request
	if(request.url == "/profile" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Welcome to your profile!')
	}

	// PUT Request
	if(request.url == "/courses" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Here’s our courses available');
	}

	// DELETE Request
	if(request.url == "/addcourse" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Add a course to our resources')
	}

	// PUT Request
	if(request.url == "/updatecourse" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Update a course to our resources');
	}

	// DELETE Request
	if(request.url == "/archivecourse" && request.method == 'GET'){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end('Archive courses to our resources')
	}
}).listen(4000);